import sourcemaps from 'rollup-plugin-sourcemaps';
import babel from 'rollup-plugin-babel';

export default {
    input: 'src/index.js',
    output: {
        file: 'dist/index.js',
        format: 'umd',
        sourcemap: true,
    },
    plugins: [
        sourcemaps(),
        babel(),
    ],
}
